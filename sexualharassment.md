# What kinds of behaviour cause sexual harassment?
## Verbal Harassment
- Commenting on clothes
- A person's body
- Sexual or gender based jokes or remarks
## Visual Harassment
- Obscene poster
- Drawings/pictures
- texts
## Physical Harassment
- Sexual Assault
- Blocking movement
- Sexual gesturing

# What would you do in case you face or witness any incident or repeated incidents of such behaviour?
## First is Safety
- If you sense that you are in danger or feel threatened, leave the situation.
- For quick assistance, look for a secure location
## Record the situations
- To Keep a thorough log of all the events, including the dates, times, places, participants, and witnesses.
- Store all proof, including messages, emails, and pictures.
## Report the Incident(s):
If comfortable and safe to do so, report the incident(s) to:
- Human Resources (if at work)
- Law enforcement (if a crime has been committed)
##  Seek Support:
- Talk to a trusted friend, family member, or counselor for emotional support.
- Seek guidance from organizations specializing in handling sexual harassment cases.
## Know Your Rights:
- Familiarize yourself with laws and policies regarding sexual harassment in your area.



