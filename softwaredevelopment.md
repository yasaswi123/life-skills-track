# Which point(s) were new to you?
- Maintaining over-communication with required members.
- Proper contacting while communication is required.
- Way of attending meeting.
- Notes making on key areas.

# Which area do you think you need to improve on? What are your ideas to make progress in that area?
- Communication is key in transferring information. I think I need to increase my communication while talking with higher positioned employes. To made progress in this regard, I need to maintain rules like talking casual when required and maintaing both professional and casual.
- Time management is an essential skill for employees in any workplace. When workers manage their time wisely, they reduce their stress levels, and get things done for your business.To made progress,Keep a to-do list with tasks and deadlines, avoid distractions such as social networking, work in intervals.