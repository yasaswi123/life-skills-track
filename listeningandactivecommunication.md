# What are the steps/strategies to do Active Listening?
- Focus on your own thoughts do not get distracted by others what they are saying.
- Don't interrupt others in the middle of their work.
- Use door openers so it will be interesting to listen.
- Body language should look as active listening.
- Take notes for important things.
- Paraphrase the sentence what others are saying so that you both are on the same page.

# According to Fisher's model, what are the key points of Reflective Listening
- Listen more than talk.
- Try to respond for personal instead of other unpersonal things.
- Understand and clarify what others are saying instead of questioning.

# What are the obstacles in your listening process?
- If speaker is talking in very low voice and in a dull mode, feels not attractive, find difficult to listen.
- If there is lot of pressure then I am unable to listen to others.
-Common distractions like heavy noises.

# What can you do to improve your listening?
- Gaining interest and focusing more on what I am doing.
- I would prefer peace environment.
- Making mind relax by taking some breaks and then start listening.

# When do you switch to Passive communication style in your day to day life?
- To avoid arguments.
- When not to hurt others by saying no.

# When do you switch into Aggressive communication styles in your day to day life?
- When someone is intentionally irritating.
- Blaming without my involvement.

# When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
- Move on with the situations sliently even though getting angry.
- Someone who does not value our feelings.

# How can you make your communication assertive?
- Speaking in a smooth tone.
- Good body language that shows respect and attentives.
- Usage of sentences in polite manner.




