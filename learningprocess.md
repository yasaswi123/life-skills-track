# What is Feynman Technique?
To able to expalin concept in simple way, to know the areas that are well understood and to know the areas which are unable to explain or stuck at any place, so that we can learn things very well and faster.
## Steps 
- To write name of concept on piece of paper
- Explain to others
- Find out stucked areas
- Try to use simple terms in the place of using complex terms.

# In this video, what was the most interesting story or idea for you?
Most interesting and inspiring story for me was becoming expert engineer from many struggles she faced, her learning techniques are also awesome like if there is any harder one splitting into small ones and understanding it this is her one technique and one more is knowing how others learned to come to that position. Overall her story was very inspiring.

# What are active and diffused modes of thinking?
Active mode is having focus on specific thing so that it understands very well.
Diffused mode is more relaxed and having bigger picture to get creative thoughts.
So we should be in switching the modes so that our ideas and problem solving skills will be great.

# According to the video, what are the steps to take when approaching a new topic? Only mention the points.
- Deconstruct the skill
- Learn enough to self correct
- Remove practice barriers
- Practice atleast 20hrs

# What are some of the actions you can take going forward to improve your learning process?
- To focus on things which I am learning for that period
- Not to destract
- Practicing and learning daily having consistency to gain the knowledge

