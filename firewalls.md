# Firewalls


# Definition of firewall
Firewall is a software code or hardware which helps to prevent our personal information or confidential information from hackers and prevent worms and viruses to system.

# Hardware firewall
Hardware firewall is a physical component that is present between network and untrusted internet to protect from hackers etc.
# When Hardware firewall used
- Protecting multiple computers
- Controlling network traffic
- VPN's for corporates 

# Software firewalls
Software firewall is already built in system , it is present mainly to protect individual systems from threats,viruses,hackers etc.
# Software firewalls used in
- Personal Computers
- Laptops
- Servers etc

# Advantages of firewalls
- High network security
- Controls traffic
- Protect from Threats,Viruses and from Unauthorized users
- Secure access to authorized users
- Blocks access to dangerous websites

# Disadvantages of firewalls
- Bad firewalls
- Cost is high
- Non Updated firewalls

# Types of firewalls
## Packet filtering firewalls
Data packets having sender and receiver and port number information. This firewall only checks that information with access controll list and allowed to system.
## Application or proxy firewalls
Proxy firewall will hide the ip addresses of system which is requested to internet , in this way it is protecting from hackers.
## Hybrid firewalls
The packet filtering and proxy firewalls connected in serial fashion to provide best security.

# References
[https://www.youtube.com/watch?v=eO6QKDL3p1I&t=44s]
[https://www.youtube.com/watch?v=aUPoA3MSajU]





